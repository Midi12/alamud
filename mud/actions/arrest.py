from .action import Action2
from mud.events import ArrestEvent

class ArrestAction(Action2):
    EVENT = ArrestEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "arrest"
