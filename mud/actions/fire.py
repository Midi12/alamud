from .action import Action2
from mud.events import FireEvent

class FireAction(Action2):
    EVENT = FireEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "fire"
