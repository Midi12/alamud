from .action import Action2
from mud.events import SmokeEvent

class SmokeAction(Action2):
    EVENT = SmokeEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "smoke"
