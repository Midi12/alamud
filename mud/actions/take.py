# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import TakeEvent, TakeInEvent

class TakeAction(Action2):
    EVENT = TakeEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "take"

class TakeInAction(Action3):
    EVENT = TakeInEvent
    RESOLVE_OBJECT = "resolve_for_take"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "take-in"
