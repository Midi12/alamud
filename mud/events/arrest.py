from .event import Event2

class ArrestEvent(Event2):
    NAME = "arrest"

    def perform(self):
        if not self.object.has_prop("arrestable"):
            self.add_prop("object-not-arrestable")
            return self.talk_failed()
        if self.object.has_prop("object-not-arrestable"):
            return self.talk_failed()

        self.inform("arrest")

    def talk_failed(self):
        self.fail()
        self.inform("arrest.failed")
