from .event import Event2

class DrinkEvent(Event2):
	NAME = "drink"

	def perform(self):
		if not self.object.has_prop("drinkable"):
			self.add_prop("object-not-drinkable")
			return self.drink_failed()
		if self.object.has_prop("object-not-drinkable"):
			return self.drink_failed()
		if self.object not in self.actor:
			return self.drink_failed()
		if self.object.has_prop("container_empty"):
			return self.drink_failed()
		self.add_prop("container_empty")
		self.inform("drink")

	def drink_failed(self):
		self.fail()
		self.inform("drink.failed")
