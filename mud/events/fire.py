from .event import Event2

class FireEvent(Event2):
    NAME = "fire"

    def perform(self):
        if not self.object.has_prop("killable"):
            self.add_prop("object-not-killable")
            return self.talk_failed()
        if self.object.has_prop("object-not-killable"):
            return self.talk_failed()

        self.inform("fire")

    def talk_failed(self):
        self.fail()
        self.inform("fire.failed")
