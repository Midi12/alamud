from .event import Event2

class SmokeEvent(Event2):
    NAME = "smoke"

    def perform(self):
        if not self.object.has_prop("smokable"):
            self.add_prop("object-not-smokable")
            return self.smoke_failed()
        if self.object.has_prop("object-not-smokable"):
            return self.smoke_failed()

        self.inform("smoke")

    def smoke_failed(self):
        self.fail()
        self.inform("smoke.failed")
