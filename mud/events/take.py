# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class TakeEvent(Event2):
    NAME = "take"

    def perform(self):
        if not self.object.has_prop("takable"):
            self.add_prop("object-not-takable")
            return self.take_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.take_failed()
        self.object.move_to(self.actor)
        self.inform("take")

    def take_failed(self):
        self.fail()
        self.inform("take.failed")

class TakeInEvent(Event3):
    NAME = "take-in"

    def perform(self):
        if not self.object.has_prop("takable"):
            self.add_prop("object-not-takable")
            return self.take_in_failure()
        if not self.object2.is_container():
            self.add_prop("object2-not-container")
            return self.take_in_failure()
        #if self.object in self.actor:
            #self.add_prop("object-already-in-inventory")
            #return self.take_in_failure()
        self.object.move_to(self.actor)
        self.inform("take-in")

    def take_in_failure(self):
        self.fail()
        self.inform("take-in.failed")
