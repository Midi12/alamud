from .event import Event2

class TalkEvent(Event2):
    NAME = "talk"

    def perform(self):
        if not self.object.has_prop("talkable"):
            self.add_prop("object-not-talkable")
            return self.talk_failed()
        if self.object.has_prop("object-not-talkable"):
            return self.talk_failed()

        self.inform("talk")

    def talk_failed(self):
        self.fail()
        self.inform("talk.failed")
