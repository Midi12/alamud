# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from mud.actions import (
    GoAction, TakeAction, LookAction, InspectAction, OpenAction,
    OpenWithAction, CloseAction, TypeAction, InventoryAction,
    LightOnAction, LightOffAction, DropAction, DropInAction,
    PushAction, TeleportAction, EnterAction, LeaveAction, DrinkAction, TalkAction, SmokeAction, TakeInAction, FireAction, ArrestAction
)

import mud.game

def make_rules():
    GAME = mud.game.GAME
    DIRS = list(GAME.static["directions"]["noun_at_the"].values())
    DIRS.extend(GAME.static["directions"]["noun_the"].values())
    DIRS.extend(GAME.static["directions"]["normalized"].keys())
    DETS = "(?:l |le |la |les |une |un |)"

    return (
        (GoAction       , r"(?:aller |)(%s)" % "|".join(DIRS)),
        (TakeAction     , r"(?:p|prendre|actio) %s([\ \-\w]+)" % DETS),
        (LookAction     , r"(?:r|regarder)"),
        (InspectAction  , r"(?:r|regarder|lire|inspecter|observer) %s([\ \-\w]+)" % DETS),
        (OpenAction     , r"ouvrir %s(\S+)" % DETS),
        (OpenWithAction , r"ouvrir %s(\S+) avec %s([\ \-\w]+)" % (DETS,DETS)),
        (CloseAction    , r"fermer %s([\ \-\w]+)" % DETS),
        (TypeAction     , r"(?:taper|[eé]crire) ([\ \-\w]+)$"),
        (InventoryAction, r"(?:inventaire|inv|i)$"),
        (LightOnAction  , r"allumer %s([\ \-\w]+)" % DETS),
        (LightOffAction , r"[eé]teindre %s([\ \-\w]+)" % DETS),
        (DropAction     , r"(?:poser|laisser) %s([\ \-\w]+)" % DETS),
        (DropInAction   , r"(?:poser|laisser) %s(\S+) (?:dans |sur |)%s([\ \-\w]+)" % (DETS,DETS)),
        (PushAction     , r"(?:appuyer|pousser|presser)(?: sur|) %s(\S+)" % DETS),
        (TeleportAction , r"t(?:ele|eleporter|p) (\S+)"),
        (EnterAction    , r"entrer"),
        (LeaveAction    , r"sortir|partir"),
		(DrinkAction    , r"boire %s(\S+)" % DETS),
		(TalkAction     , r"parler avec %s(\S+)" % DETS),
		(SmokeAction 	, r"fumer %s(\S+)" % DETS),
		(TakeInAction   , r"(?:p|prendre) %s(\S+) (?:dans |sur |)%s([\ \-\w]+)" % (DETS,DETS)),
		(FireAction     , r"tirer (?:sur |)%s([\ \-\w]+)" % (DETS)),
		(ArrestAction   , "(?:arreter|attraper|menotter) %s([\ \-\w]+)" % (DETS))
    )
