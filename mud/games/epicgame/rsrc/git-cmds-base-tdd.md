Alice:
======
Paramétrage et initialisations:
-------------------------------
- git config --global user.name "Alice Torvalds"
- git config --global user.email "alice@kernel.org"

Création d'un dossier local versionné
-------------------------------------

mkdir monprojet
cd monprojet
git init
Si vous avez déja du contenu:

git add .

Création d'un dépot "monprojet" sur gitlab
-------------------------------------------

Connexion entre le local et le gitlab:
--------------------------------------

git remote add origin https://gitlab.com/alice/monprojet.git

Publication sur le remote (gitlab):
---------------------------------------
Faire qqes modifs, commiter puis pour publier:
git push -u origin master

Alice ajoute Bob en tant que Reporter sur son projet

Bob forke le Projet d'Alice sur Gitlab puis clone le projet ainsi obtenu.
Il ajoute ensuite le remote d'Alice:
-----------------------------------------
git remote add alice https://gitlab.com/alice/monprojet.git
et vérifie qu'il a bien 2 remotes (le sien et celui d'Alice) en tapant:

git remote -v

Réalisation d'un item de backlog  par Alice :
-----------------------------------------------------
- Alice prend un item de backlog et implémente le code correspondant. pour démarrer, peut juste ajouter un Readme.md par exemple
- teste si nécessaire
- git commit -am "Message de commit"

Alice pousse son master sur son remote:
---------------------------------------
 - git push -u origin master
 - ou simplement : git push

Bob:
====
Récupère le master d'Alice:
----------------------------
 - git fetch Alice master

éventuellement consulte la branche locale correspondant au master d'Alice:
-------------------------------------------
consulte la liste des branches disponibles:
 - git branch -av
 se place dans la branche d'Alice:
 - git checkout Alice/master
fait sa revue de code ...
puis si il est satisfait, revient dans son master:
-----------------------
 - git checkout master

puis merge le travail d'Alice et pousse les modifs dans son dépôt distant:
---------------------------------------------------
 - git merge Alice/master
 - git push
 - detruit la branche locale d'Alice :
 	- branch -d Alice/master

Bob:
=====
Travaille à son tour fait qqes modifs

Alice:
======
- fetche le master de Bob pour se mettre à jour:
	- git fetch Bob master
- Fusionne:
	- git merge Bob/master

ou pour aller plus vite :
---------------------------

git pull Bob master

qui fera la récupération ET le merge

Conservation du passwd gitlab (ou github)
================================================
* Soit de façon permanente dans un fichier .netrc mais passwd en clair
* Soit de façon temporaire (ici 1h):
	git config --global credential.helper 'cache --timeout=3600'
